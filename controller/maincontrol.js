const app = require('../app')
const user = require('./user')


class controller {

    static postHome(req, res, next) {
        try {
           res.status(200).send(`post Home Page`)
        } catch(error) {
           console.log(error)
           next(error)
        }
     }
  
     static deleteHome (req, res, next) {
        try {
           res.status(200).send(`Delete Home Page `)
        } catch(error) {
           console.log(error)
           next(error)
        }
     }
  
     static putHomePage (req, res, next) {
        try {
           res.status(200).send(`Put Home Page `)
        } catch(error) {
           console.log(error)
           next(error)
        }
     }
  
  
     static loginPage (req,res,next) {
        try {
           res.render('login')
        } catch(error) {
           console.log(error)
           next(error)
        }
     }


    static loginGate(req, res) {
        const _user = user.getUser()
        const query = req.query.name
        const passWord = req.query.password
        const identity = _user.find(user => user.name === query)
   

    if(!identity) {
        return res.status(404).redirect('/error')
    }

    const _passWord = identity.password
    if(identity && passWord === _passWord){
        return res.status(300).redirect("/dashboard")
    }
    return res.status(400).redirect('/error')

}


}




module.exports  = controller
module.import = user

