create  table user_game (
	id serial primary key,
	user_name VARCHAR (20),
	password VARCHAR (20)
)

create table user_game_biodata (
	id serial primary key,
	first_name VARCHAR (20),
	last_name VARCHAR (20),
	address varchar (20),
	age varchar (3)
)

create table user_game_history (
 	id serial primary key,
 	time_play varchar (10),
 	date_play varchar (10),
 	credit_value varchar (20)
)

create  table user_game (
	id serial primary key,
	user_name VARCHAR (20),
	password VARCHAR (20)
)

create table user_game_biodata (
	id serial primary key,
	first_name VARCHAR (20),
	last_name VARCHAR (20),
	address varchar (20),
	age varchar (3)
)

create table user_game_history (
 	id serial primary key,
 	time_play varchar (10),
 	date_play varchar (10),
 	credit_value varchar (20)
)


insert into user_game  (name, password, age)
values ('joni', '123', '32');

insert into user_game  (name, password, age)
values ('budi', '457', '36');

insert into user_game id=1  (age)
values ('32');

insert into user_game where id=2 (age)
values ('36');

insert into user_game  (name, password)
values ('budi', '457');


insert into user_game_biodata (first_name, last_name, address, age)
values ('joni', 'sihula hula', 'jalanan', '32')

insert into user_game_biodata (first_name, last_name, address, age)
values ('budi', 'pekerti', 'perumahan elite', '36')


insert into user_game_history (time_play, date_play, credit_value, last_name)
values ('1000 HOURS', '20-11-1990', '1.000.000,00', 'pekerti')

insert into user_game_history (time_play, date_play, credit_value, last_name)
values ('2000 HOURS', '20-11-1997', '6.000.000,00', 'sihula hula')
 
update user_game set password='234' where id=1

update user_game set password='784' where id=2

update user_game_biodata set age ='36' where id=1

update user_game_biodata set age ='32' where id=2

update user_game_history  set id ='1' where id=5

update user_game_history  set id ='2' where id=6

update user_game set id ='1' where id=6



delete from user_game_biodata  where id=1

delete from user_game_biodata  where id=2

delete from user_game where id=7

delete from user_game where id=8

ALTER TABLE user_game  RENAME COLUMN user_name TO name

ALTER TABLE user_game  add column age varchar(20);

ALTER TABLE user_game_history  add column last_name varchar(20);






