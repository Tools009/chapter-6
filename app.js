const express = require('express')
const app = express()
const PORT = 3000
const path = require('path')
const controller = require('./controller/maincontrol')
const router = require('./routes/login')
const { route } = require('./routes/login')
const middleware = require('./middleware/middleware.js')
const dataPath = path.join(__dirname,'data')
const client = require('./data/database.js')



app.set("view engine", "ejs")
app.post('/')

const viewPath = path.join(__dirname,'view')

app.set("views", viewPath)

app.get('/', (req, res) => {
    res.render('login')
} )

app.get('/dashboard', (req, res) => {
    res.render('welcome')
})

app.get('/error', (req,res) => {
    res.render('error')
})

app.use('/', router)

app.use(express.static(path.join(__dirname, 'public')));


app.use('/javascript', express.static(path.join(__dirname, 'click')));

app.listen(PORT, () => console.log(`http://localhost:${PORT}`))

app.use(middleware)


app.set("data", viewPath)

client.connect()



//postgresql RESTFULL API

//GET ALL USER

app.get ('/users', (req, res) => {
    client.query(`SELECT * from user_game`, (err, result) => {
        if(!err) {
            res.send(result.rows)
        }
    })
    client.end
})


//GET USER BY ID FROM POSTGRESQL

app.get ('/users/:id', async(req, res) => {
    const { id } = req.params 
    try {
        const users_game = await client.query(`SELECT * from user_game where id = $1`, [id])

        res.json(users_game.rows)
    } catch (error) {
        
    }
})

//GET BIODATA USER FROM POSTGRESQL

app.get ('/biodata', (req, res) => {
    client.query(`SELECT * from user_game_biodata`, (err, result) => {
        if(!err) {
            res.send(result.rows)
        }
    })
    client.end
})

//GET USER HISTORY GAME FROM POSTGRESQL

app.get ('/history', (req, res) => {
    client.query(`SELECT * from user_game_history`, (err, result) => {
        if(!err) {
            res.send(result.rows)
        }
    })
    client.end
})


app.use(express.json())


// CREATE

app.post('/users/newid', async(req, res)=> {
    try {

        const {name, password, age, id} = req.body
        const newUsergame = await client.query("INSERT into user_game (name, password, age, id) values ($1, $2, $3 , $4) Returning * ", [name, password, age, id])
    res.json(newUsergame)


    }catch(err) {
        console.log(error.message);
    }
})

// UPDATE USER


app.put ('/users/:id', async(req, res) => {
 
    try {
        const { id } = req.params
        const { name, password, age } = req.body

        client.query(`UPDATE user_game SET name = $1, password = $2, age = $3 WHERE id = $4`, [name, password, age, id])

        res.json("data is updated")
    } catch (error) {
        
    }
})


// DELETE USER

app.delete ('/users/:id', async(req, res) => {
   
    try {
        const { id } = req.params
        const { name } = req.body
        const deleteUser = await client.query(`DELETE FROM user_game WHERE id = $1`, [id])

        res.json("data is delete")
    } catch (error) {
        
    }
})



module.import = client
module.import = router
module.import = controller
module.import = middleware


module.exports = app